#!/bin/bash

############################################################
# Parse Arguments and Flags                                #
############################################################

CONFIG="config.sh"
MODE="pull"

while getopts "c:m:" option; do
	case "${option}"
	in
		c) CONFIG="config.${OPTARG}.sh";;
		m) MODE="${OPTARG}";;
	esac
done

############################################################
# Check for our requirements                               #
############################################################

declare -a deps=(mysql mysqldump sed grep lftp)

for i in ${deps[@]}; do
	type -P $i >/dev/null 2>&1 || { 
		echo >&2 "${i} not available. Aborting.";
		exit 1;
	}
done

source ${0%/*}/$CONFIG > /dev/null 2>&1 || {
	echo >&2 "Config file not found at: ${0%/*}/${CONFIG}";
	exit 1;
}

if [ "$MODE" != "pull" ] && [ "$MODE" != "push" ] && [ "$MODE" != "snapshot" ]; then
	echo >&2 "Mode should be either 'pull' (default), 'push', or 'snapshot'."
	exit 1;
fi

############################################################
# FTP Transfer                                             #
############################################################
[ "$MODE" == "pull" ] && TEXT="Update Destination Files"
[ "$MODE" == "snapshot" ] && TEXT="Download Source Files"
[ "$MODE" == "push" ] && TEXT="Update Source FTP"

read -p "${TEXT} (y/n)? "
[ "$REPLY" != "y" ] || {
	if [ "$MODE" == "pull" ] || [ "$MODE" == "snapshot" ]; then
	lftp -f "
		set ftp:list-options -a
		open ftp://${SOURCE_FTP_SERV}
		user ${SOURCE_FTP_USER} ${SOURCE_FTP_PASS}
		mirror --verbose=2 --parallel=3 ${SOURCE_FTP_PATH} ${DEST_FILE_PATH}
		bye
	"
	fi
	
	if [ "$MODE" == "push" ]; then
	lftp -f "
		open ftp://${SOURCE_FTP_SERV}
		user ${SOURCE_FTP_USER} ${SOURCE_FTP_PASS}
		mirror --parallel=3 --reverse --verbose=2 -X 'wp-config.php' -X '.git/' ${DEST_FILE_PATH} ${SOURCE_FTP_PATH}
		bye
	"
	echo "NOTE: wp-config.php was excluded to preserve remote settings."
	fi
}
############################################################
# If Pulling, Read wp-config: Source Database Variables    #
############################################################
if [ -e $DEST_FILE_PATH/wp-config.php ] && ([ "$MODE" == "pull" ] || [ "$MODE" == "snapshot" ]); then
	eval $(awk -F"[()']" '/^define/{printf "%s=\"%s\"\n", $3, $5;}' < ${DEST_FILE_PATH}/wp-config.php | grep "DB_" | sed s/DB_/SOURCE_WPCDB_/g )

	[[ -z $SOURCE_DB_NAME ]] && SOURCE_DB_NAME=$SOURCE_WPCDB_NAME
	[[ -z $SOURCE_DB_USER ]] && SOURCE_DB_USER=$SOURCE_WPCDB_USER
	[[ -z $SOURCE_DB_PASSWORD ]] && SOURCE_DB_PASSWORD=$SOURCE_WPCDB_PASSWORD
	[[ -z $SOURCE_DB_HOST ]] && SOURCE_DB_HOST=$SOURCE_WPCDB_HOST
fi

############################################################
# Database Dump, Translate, and Load                       #
############################################################
[ "$MODE" == "pull" ] && TEXT="Load Destination Database from Source. DB will be *overwritten*"
[ "$MODE" == "snapshot" ] && TEXT="Dump Source Database to File"
[ "$MODE" == "push" ] && TEXT="Dump Destination Database to Source. DB will be *overwritten*"

read -p "${TEXT} (y/n)? "
[ "$REPLY" != "y" ] || {
	if [ "$MODE" == "pull" ]; then
	mysqldump -h $SOURCE_DB_HOST \
	--user=$SOURCE_DB_USER \
	--password=$SOURCE_DB_PASSWORD \
	--opt \
	$SOURCE_DB_NAME |
	
	sed s/${SOURCE_URL_ESC}/${DEST_URL}/ig |
	
	mysql -h $DEST_DB_HOST \
	--user=$DEST_DB_USER \
	--password=$DEST_DB_PASSWORD \
	$DEST_DB_NAME;
	fi
	
	if [ "$MODE" == "snapshot" ]; then
	mysqldump -h $SOURCE_DB_HOST \
	--user=$SOURCE_DB_USER \
	--password=$SOURCE_DB_PASSWORD \
	--opt \
	$SOURCE_DB_NAME > ${DEST_FILE_PATH}/wp-database.sql
	fi
	
	if [ "$MODE" == "push" ]; then
	mysqldump -h $DEST_DB_HOST \
	--user=$DEST_DB_USER \
	--password=$DEST_DB_PASSWORD \
	--opt \
	$DEST_DB_NAME |
	
	sed s/${DEST_URL}/${SOURCE_URL_ESC}/ig |

	mysql -h $SOURCE_DB_HOST \
	--user=$SOURCE_DB_USER \
	--password=$SOURCE_DB_PASSWORD \
	$SOURCE_DB_NAME;
	fi
	
	
}
############################################################
# If Pulling, Update wp-config: New Destination Variables  #
############################################################
if [ -e $DEST_FILE_PATH/wp-config.php ] && [ "$MODE" == "pull" ]; then
	read -p "Update Destination wp-config with Destination DB Config? (y/n)? "
	[ "$REPLY" != "y" ] || {
	cp $DEST_FILE_PATH/wp-config.php $DEST_FILE_PATH/../wp-config.original.php
	echo "Backup wp-config saved 1 dir above new site root as wp-config.original.php"
	
	sed -i.backup \
		-e "s/define('DB_NAME', '${SOURCE_WPCDB_NAME}');/define('DB_NAME', '${DEST_DB_NAME}');/g" \
		-e "s/define('DB_USER', '${SOURCE_WPCDB_USER}');/define('DB_USER', '${DEST_DB_USER}');/g" \
		-e "s/define('DB_PASSWORD', '${SOURCE_WPCDB_PASSWORD}');/define('DB_PASSWORD', '${DEST_DB_PASSWORD}');/g" \
		-e "s/define('DB_HOST', '${SOURCE_WPCDB_HOST}');/define('DB_HOST', '${DEST_DB_HOST}');/g" \
		$DEST_FILE_PATH/wp-config.php
	}
fi