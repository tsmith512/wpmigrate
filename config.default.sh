#!/bin/bash

############################################################
# Site Setting for Remote Source                           #
############################################################
SOURCE_FTP_USER="dutchess"
SOURCE_FTP_PASS="guest"
SOURCE_FTP_SERV="ftp.isis.org"
SOURCE_FTP_PATH="/flexaccount" # Include initial slash

# wget's Cut Directories option. Set to the number of
# directories in the source path for a direct copy:
# ftp.server.com/web/site/files/[index.html & files]
#                ^   ^    ^
#                3   2    1
# If you want the site's files in the site root you set at
# DEST_FILE_PATH, then you need to make sure this option is
# set to the number of directories in that path, 3 in this
# example. Sorry, blame wget.
SOURCE_FTP_CUTD="1"

SOURCE_URL_ESC="www.isis.org" # Hostname. Path if needed.

# Source DB defaults taken from <site-root>/wp-config.php
# Overide here as necessary
SOURCE_DB_NAME=""
SOURCE_DB_USER=""
SOURCE_DB_PASSWORD=""
SOURCE_DB_HOST="remoteaccess.mydatabases.com"

############################################################
# Site Settings for Local Destination                      #
############################################################
DEST_FILE_PATH="/home/devloper/public_html" # Absolute path 
DEST_URL="test.buildserver.in" # Hostname and path

# The assumption is that source data won't work here, so
# these variables take no default values. Set them all.
DEST_DB_NAME="developer"
DEST_DB_USER="developer"
DEST_DB_PASSWORD="password"
DEST_DB_HOST="localhost"

